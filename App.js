import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useMemo, useState} from 'react';
import Image1 from './Image1.png';
import Image2 from './Image2.png';
import Image3 from './Image3.png';

const DummyData = [
  {
    id: 0,
    image: Image1,
    like: 0,
  },
  {
    id: 1,
    image: Image2,
    like: 0,
  },
  {
    id: 2,
    image: Image3,
    like: 0,
  },
];

const App = () => {
  const [data, setData] = useState(DummyData);

  const onLike = value => {
    let tmp = data;
    const item = data.find(i => i == value);
    const idx = data.indexOf(item);
    tmp[idx].like = data[idx].like + 1;
    setData([...tmp]);
  };

  const onDislike = value => {
    let tmp = data;
    const item = data.find(i => i == value);
    const idx = data.indexOf(item);
    if (data[idx].like > 0) {
      tmp[idx].like = data[idx].like - 1;
    }
    setData([...tmp]);
  };

  const onReset = () => {
    setData(DummyData);
  };

  const onLikeAll = () => {
    let tmp = data.map(item => ({
      id: item.id,
      image: item.image,
      like: item.like + 1,
    }));
    setData([...tmp]);
  };

  const onDislikeAll = () => {
    let tmp = data.map(item => ({
      id: item.id,
      image: item.image,
      like: item.like === 0 ? 0 : item.like - 1,
    }));
    setData([...tmp]);
  };

  const renderPost = ({item, index}) => {
    return (
      <View style={styles.post}>
        <View style={styles.postImage}>
          <Image style={styles.image} source={item.image} />
        </View>
        <View style={styles.buttonContainer}>
          <View style={styles.like}>
            <Text style={styles.likeTxt}>{item.like} Like</Text>
          </View>
          <View style={styles.likeContainer}>
            <TouchableOpacity
              onPress={() => onLike(item)}
              style={styles.likeButtonBottom}>
              <Text style={styles.likeButtonTxt}>Like</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => onDislike(item)}
              style={styles.dislikeButtonBottom}>
              <Text style={styles.dislikeButtonTxt}>Dislike</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerMenu}>
        <TouchableOpacity onPress={onLikeAll} style={styles.likeButton}>
          <Text style={styles.likeButtonTxt}>Like All</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={onReset} style={styles.resetButton}>
          <Text style={styles.resetButtonTxt}>Reset All</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={onDislikeAll} style={styles.dislikeButton}>
          <Text style={styles.dislikeButtonTxt}>Dislike All</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.body}>
        <FlatList
          data={data}
          renderItem={renderPost}
          keyExtractor={(_, i) => i.toString()}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.list}
        />
      </View>
    </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F4F4F4',
    paddingVertical: 15,
    paddingHorizontal: 12,
  },
  headerMenu: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  likeButton: {
    backgroundColor: '#2B72C4',
    borderRadius: 10,
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 25,
  },
  likeButtonTxt: {
    fontSize: 16,
    color: '#FFFFFF',
  },
  resetButton: {
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 25,
    elevation: 1,
  },
  resetButtonTxt: {
    fontSize: 16,
    color: '#5F5F5F',
  },
  dislikeButton: {
    backgroundColor: '#DB2C2C',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  dislikeButtonTxt: {
    fontSize: 16,
    color: '#FFF',
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
  postImage: {
    width: '75%',
    height: undefined,
    aspectRatio: 4 / 3,
    alignItems: 'center',
    alignSelf: 'center',
  },
  post: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: '#D5D5D5',
    marginBottom: 15,
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 11,
    paddingRight: 20,
  },
  like: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: '#D5D5D5',
    paddingVertical: 6,
    paddingHorizontal: 10,
    marginLeft: 14,
    borderRadius: 5,
  },
  likeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  likeButtonBottom: {
    backgroundColor: '#2B72C4',
    borderRadius: 5,
    paddingHorizontal: 25,
    paddingVertical: 5,
    marginRight: 10,
  },
  dislikeButtonBottom: {
    backgroundColor: '#DB2C2C',
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  list: {
    paddingTop: 15,
    paddingBottom: 30,
  },
});
